package main;

public class DiscrepancyException extends RuntimeException{
	private static final long serialVersionUID = -2920670722114622878L;
	public DiscrepancyException(String message) {
		super(message);
	}
}
